using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class Camera_Plugin_Handler : MonoBehaviour
{
	private static Camera_Plugin_Handler instance;

	// Import Plugin Method
	[DllImport ("__Internal")]
	private static extern void _launchCamera(string gameObjectName);


	// Call Plugin Method
	public static void start(string gameObjectName)
	{
		_launchCamera(gameObjectName);
	}
}
